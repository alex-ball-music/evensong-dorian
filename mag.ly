global = {
  \key d \dorian
  \time 4/4
  \tempo 4=120
}

saControl = {
  s1 s1
  s1 s2. s4\f \time 3/4
  s2. \time 4/4 s1
  s1 s1 \time 2/4
  s2 \time 4/4 s1 \time 3/4
  s2. s2 s4\mp
  s2. s2.
  s2. s2.
  s2. s4 s2\f
  s2. \time 4/4 s1
  s1 \time 3/4 s2. \time 4/4
  s1 \time 3/4 s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2. \time 4/4
  s1*4
  \bar "||" \mark\default
  s1 s2 s2\mp
  s1 s1
  s1 s2 \crescTextCresc s2\<
  s1 s1
  s1\mf s1
  s1 s1
  s2 s2\mf\< \time 3/4
  s2. \time 6/4 s1. \time 4/4
  s1\ff s1 \crescHairpin
  \bar "||" \mark\default
  s1 s1 \time 5/4
  s2.\f s2 \time 4/4 s1 \time 3/4 s2.
  s2.\mp s2.
  s2. s2. \time 4/4
  s1\mf s1
  s2 s2\mp
  s1 s1\> \time 3/4
  s2.\p s2.
  s2. s2.
  \bar "||" \mark\default
  s2.\mf \time 4/4 s1 \time 3/4
  s2. s2.\< s2.\>
  s2.\! s4 \crescTextCresc s2\<
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2.\ff s2.
  s2. s2.
  \bar "||" \mark\default
}

tbControl = {
  s1 s1
  s1 s2. s4\f \time 3/4
  s2. \time 4/4 s1
  s1 s1 \time 2/4
  s2 \time 4/4 s1 \time 3/4
  s2. s2 s4
  s2. s2.
  s2. s2.
  s2. s4 s2\f
  s2. \time 4/4 s1
  s1 \time 3/4 s2. \time 4/4
  s1 \time 3/4 s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2. \time 4/4
  s1*4

  s1 s2 s2
  s1 s1
  s1 s2 s2
  s1 s1
  s1 s1
  s1\f s1 \crescTextCresc
  s2 s2\mf\< \time 3/4
  s2. \time 6/4 s1. \time 4/4
  s1\ff s1 \crescHairpin

  s1 s1 \time 5/4
  s2.\f s2 \time 4/4 s1 \time 3/4 s2.
  s2.\mp s2.
  s2. s2. \time 4/4
  s1 s1
  s2 s2\mp
  s1 s1\> \time 3/4
  s2.\p s2.
  s2. s2.

  s2.\mf \time 4/4 s1 \time 3/4
  s2. s2.\< s2.\> \crescTextCresc
  s2.\! s4 s2\<
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.\ff
  s2. s2.
}

pnControl = {
  s1\f s1
  s1 s2. s4 \time 3/4
  s2. \time 4/4 s1
  s1 s1 \time 2/4
  s2 \time 4/4 s1 \time 3/4
  s2. s2\> s4
  s2.\mp s2.
  s2. s2.
  s2.\< s4\f s2
  s2. \time 4/4 s1
  s1 \time 3/4 s2. \time 4/4
  s1 \time 3/4 s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.\> \time 4/4
  s1\mp s2 s2
  s1*2

  s1*2
  s1 s1 \crescTextCresc
  s1 s2 s2\<
  s1 s1
  s1\mf s1
  s1\f s1
  s2 s2\mf\< \time 3/4
  s2. \time 6/4 s1. \time 4/4
  s1\ff s1 \crescHairpin

  s1\f s1 \time 5/4
  s2. s2 \time 4/4 s1 \time 3/4 s2.\>
  s2.\mp s2.
  s2. s2.\< \time 4/4
  s1\mf s1
  s2\> s2\mp
  s1 s1\> \time 3/4
  s2.\p\< s2.
  s2. s2.

  s2.\mf \time 4/4 s1 \time 3/4
  s2. s2.\< s2.\>
  s2.\! s4 s2\cresc
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2. s2.
  s2.\ff s2.
}


soprano = \relative c' {
  \global
  % Music follows here.
  R1*3  r2. d4
  a'2 f4  b4 b g g
  a2 a4 g  a4 g a b
  c d  c( b) a( g)
  a2 a4 r2 a4
  c4 d c b a g
  c d c b a g
  a a r r d, d
  a'2 f4  b2 g
  a g4 a  b c d
  c( b) a( g)
  a2. r2 a4
  c4 d c b a g
  c4 d c b2 a4
  c2 c4 b g2
  a2. R2.
  R1*4
  R1 r2 d,4 d
  a'4 a g a b2 g
  a d, r2 d
  a' g4( a) b2 g
  d' cs R1
  R1*2
  r2 a4 a
  b4 a a  c a d a e' a,
  f' f e d e2. r4
  R1*2
  d4 a f b a  b g d b'  a2 r4
  d,4 e f  e f g
  a b g  a2 r4
  d4 a f d'  b g c b
  a2 d,4 e  f e f a
  g2 f4 d  e2. R2.*3
  a2 a4 b b g g a a b c d c b( a) g a4. a8 a4
  r4 a4 b
  c c d b a( g)
  a a2 r4 a4 b
  c d c b2 g4
  a2.( d)
  cs ~ cs2.
  R2.*2
}

alto = \relative c' {
  \global
  % Music follows here.
  R1*3 r2. d4
  d2 d4 d d d e
  f2 f4 e f e f g
  a4 b a( g) f( e)
  e2 e4 r2 a4
  a a a g g g
  a a a g a g
  a a r4 r d, d
  d2 d4 d2 d4( e)
  f2 e4 f g
  a4 b a( g) f( e)
  e2. r2 a4
  a a a g g g
  a a a g2 g4
  a2 a4 b4 g2
  a2. R2.
  R1*4
  R1 r2 d,4 d
  a'4 a g a b2 g
  a d, r2 d
  a' g4( a) b2 g
  a a R1
  R1*2
  r2 a4 a
  g4 a a g a f a e a
  d, d e f a2. r4
  R1*2
  f4 d a d f g d b g' f2 r4
  d4 e f e f g a b g a2 r4
  a4 f d a' g g g g
  f2 d4 d d d d d
  d2 f4 d d2. R2.*3
  d2 d4 d d d e
  f4 f g a b a
  g( f) e e4. e8 e4
  r4 a a
  a4 a b g f( g)
  a4 a2 r4 a a
  a4 b a g2 g4
  f2.( a2.)
  a2. ~ a2.
  R2.*2
}

tenor = \relative c {
  \global
  % Music follows here.
  R1*3 r2. d4
  f2 a4 g d' c b
  a2 d4 e d d c c
  c g a2 b
  cs2 cs4 R2.
  R2.*5 r4 d, d
  f2 a4 g2 c4( b)
  a2 d4 d
  d4 d d f2 d
  cs2. r2 e4
  f f f d d d
  f f f d2 d4
  f2 f4 d( c) b
  a2. R2.
  R1*4
  R1*10
  <d d,>2 <d d,>4 <d d,> <g g,>2 <f f,>4 <e e,>
  <d d,>2 a4 a
  a a a a a a a a a
  a a a a a2. r4
  R1*2
  d4 a f b a d b g d' d2 r4
  d,4 e f e f g a b g a2 r4
  R1*2
  r2 d,4 d a' a a a
  b2 g4 b a2. R2.*3
  f2 a4 g d' c b
  a4 c d c g a
  b2 b4 cs4. cs8 cs4
  r4 c! d c c b
  d4 d( b) cs cs2
  r4 c! d c c d
  d2 d4 r4 d f
  r d f r e e e2.
  R2.*2
}

bass = \relative c {
  \global
  % Music follows here.
  R1*3 r2. d4
  d2 d4 g,4 g b b
  d2 d4 a d d f f
  c c f,2 g
  a2 a4 R2.
  R2.*5 r4 d d
  d2 d4 g,2 b2
  d2 d4 d
  g,4 a g f2 g
  a2. r2 a'4
  f a f g d g
  f a f g2 d4
  f2 c'4 b4 g2
  a2.R2.
  R1*4
  R1*10
  d,2 d4 d g2 f4 e
  d2 a4 a
  a a a a a a a a a
  a a a a a2. r4
  R1*2
  d4 a a d a b g d' b d2 r4
  d4 e f e f g a b g a2 r4
  R1*2
  r2 d,4 d d d d d
  d2 d4 d a2. R2.*3
  d2 d4 g,4 g b b
  d d d f f f
  g2 g4 a4. a8 a4
  r4 a g f f f
  g4 g2 a4 a2
  r4 a g f f f
  g2 g4 r4 a d
  r a d r cs cs cs2.
  R2.*2
}

saVerse = \lyricmode {
  % Lyrics follow here.
  My soul doth mag -- ni -- fy the Lord,
  and my spir -- it hath re -- joiced in God my Sa -- viour.
  For he hath re -- gard -- ed
  the low -- li -- ness of his hand -- maid -- en.
  For be -- hold, from hence -- forth
  all gen -- er -- a -- tions shall call me blest.
  For he that is migh -- ty hath mag -- ni -- fied me
  and ho -- ly is his Name.
  And his mer -- cy is on them that fear him
  through -- out all gen -- er -- a -- tions.
  he hath scat -- tered the proud in the i -- ma -- gin -- a -- tion of their hearts.
  He hath put down the migh -- ty from their seat,
  and hath ex -- al -- ted the hum -- ble and meek.
  He hath filled the hun -- gry with good things,
  and the rich he hath sent emp -- ty a -- way.
  He re -- mem -- ber -- ing his mer -- cy hath holp -- en his ser -- vant Is -- ra -- el,
  as he prom -- ised to our fore -- fath -- ers, Ab -- ra -- ham and his seed for e -- ver.
}

tbVerse = \lyricmode {
  % Lyrics follow here.
  \repeat unfold 52 { \sh }
  He hath shewed strength with his arm;
  \repeat unfold 79 { \sh }
  e -- ver, e -- ver, e -- ver -- more.
}

right = \relative c''' {
  \global
  % Music follows here.
  d4 a f8 a d a c( b) a g a( g) f e
  d4 a f8 a d a c( b) a g f e d4
  <f a>4 f <a d> <b g> g d <g b>
  a f a d <a f> <g e> <a f> <b g>
  <c a> <d b> <c a> <b g> <a f> <g e>
  cs, e a cs, e a
  << { c4 d c b a b } \\ { <f a>2. <d g> } >>
  << { c'4 d c b a b } \\ { <f a>2. <d g> } >>
  <e a cs>2 q4 <e g d'>2 q4
  <f a>4 f <a d> <b g> g d <g b>
  a f <g e> <a f> <b g>
  <c a> <d b> <c a> <b g> <a f> <g e>
  cs, e a cs, e a
  << { c4 d c b a b } \\ { <f a>2. <d g> } >>
  << { c'4 d c b b a } \\ { <f a>2. <d g> } >>
  << { c'4 f e d c b } \\ { <f a>2. <d g> } >>
  << { a'4 g f <e a,>2. } \\ { e4 d cs ~ cs2. } >>
  f4 a, <d f> a r2 d'2
  a'2 g4 a b4 c8( b) a4 g
  <f a,>4 <e g,> <d f,> <c e,> <d f,> <c e,> <b d,>2
  a4 d, <f a> d b'4 d, <g b> d
  a'4 d, <f a> d r4 d <f a> d
  a'4 d, <f a> d b'4 d, <g b> d
  <a' d>4 e <a cs> e <a b>4 e <a cs> e4
  <d a' d>2 q4 q
  <g d>2 <f c>4 <e b>
  <d a>2 a'4 a
  <b g>4 a a <c g> a <d f,> a <e' e,> a,
  <f' d a>4 q <e a,> <d f,> <e a,>2. r4
  d4 a f8 a b a b4 g d b'
  <d f,>4 <a d,> <f a,> <b d,> <a f>  <b g d> <g d b> <d b> <b' g d>  a4 f d
  <d a>4 <e c> <f d> <e c> <f d> <g e>
  <a f> <b g> <g d>4 <a cs,>2.
  <d a f>4 <a f> f <d' a> <b g d> g <c g> <b g>
  <a f c>2 d,4 <d e>
  << { f4 e f a } \\ { d,2 d2 } >>
  <g b,>4 d <f b,> d
  <d e a>4\staccato q\staccato q\staccato
  <d f c'>\staccato <e f b>\staccato <d f a>\staccato
  <d e a>4\staccato q2
  <cs e a>2.
  << { r4 <f a>4. r8 r4 <g b> r <g b> } \\ { d2 d4 d2 d2 } >>
  << { r4 <f a>4. r8 r4 <f a>4. r8 } \\ { d2 d4 c2 c4 } >>
  <g' b>4 <f a> <e g>
  << { r4 <a cs>4. r8 r4 <a c!>4. r8 } \\ { e2 e4 e2 e4 } >>
  << { r4 <f a>4. r8 } \\ { c2 c4 } >> <g' b>4 <f a> <e g>
  << { r4 <a cs>4. r8 r4 <a c!>4. r8 } \\ { e2 e4 e2 e4 } >>
  << { r4 <d' b> <a c> r <b d>4. r8 } \\ { f2 f4 g2 g4 } >>
  << { r4 <f a> <a d> r4 <f a> <a d> } \\ { d,2. d2. } >>
  e4 <a cs>8 e <a cs>4 e <a cs>8 e <a cs>4
  e <a cs>4 <a cs>4 <e a cs>2.
}

left = \relative c' {
  \global
  % Music follows here.
  d1 <g, d'>2 q2
  <d a' d>1 <g, d' g>2 q2
  <d' a' d>2. <g, d' g>2 q2
  <d' a' d>1 <d a' d>2 <f c' f>
  <c g' c> <f, c' f> <g d' g>
  <a e' a>2. <a e' a>2.
  f'4 a f g d g
  f a f g d g
  <a a,>2. q2.
  <d, a' d>2. <g, d' g>2 q2
  <d' a' d>1  <g, d' g>2.
  <f c' f>2 <g d' g>
  <a e' a>2. <a e' a>2.
  << {
    f'4 a f g d g
    f a f g d g
    f4 a c b g2
  } \\  {
    f,2._\markup{ \italic "ped." } g2.
    f2. g2.
    f2. g2.
  } >>
  <a a'>2_\markup{ \italic "man." } q4 a b cs
  d1 << {
    d'4 b <d g> b
    f'4 a, <d f> a d4 b <d g> b
    f'4 a, <d f> a d4 g, b g
  } \\ {
    g,1_\markup{ \italic "ped." }
    d'1 g,1
    d'1 g,1
  } >>
  d'1_\markup{ \italic "man." } g,1
  d'1 d1
  d1 g,1
  << {
    a4 e' a e a g f e
  } \\ {
    a,1_\markup{ \italic "ped." } ~
    a1
  } >>
  <d a d,>2 q4 q
  <g d g,>2 <f c f,>4 <e b e,>
  <d a d,>2
  << {
    <a a'>2
    q2.  q2 q2 q2
    q1 q2. r4
  } \\ {
    a,2_\markup{ \italic "ped." } ~
    a2. ~ a1. ~
    a1 ~ a2. r4
  } >>
  <d' d'>1 <g, g'>2 q2
  <d' d'>4 <a a'> <a f'> <d b'> <a a'> d g, <d' g> b <d a'>2.
  <d a' d>2. <c g' c>
  f,8 a c g b d a4 ~ <a e'> ~ <a e' a>
  d4 a' d a8 d, g,4 d' g d8 g,
  f4 f' d a <d a'>2 q2
  <d g,>2 q2
  <a a'>2. ~ <a a'>2. ~
  <a a'>2. a4 g e
  <d d'>2.
  g2 b2 d2 d,4
  f2. g
  a2. a2 g4
  f2. g2.
  a2. a2 g4
  f2. g2.
  a2. f2.
  <a a'>2 <bf bf'>4 <a a'>2 <g g'>4
  <a a'>2. <a a'>4 <g g'> <e e'>
}

choirPart = \new ChoirStaff <<
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano" { \voiceOne \soprano \sgloria }
    \new Voice = "alto" { \voiceTwo \alto \agloria }
    \new Voice { \dynamicUp \saControl \sagloriaControl }
  >>
  \new Lyrics = "sa-lyrics" \lyricsto "alto" { \saVerse \sagloriaVerse }
  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor \tgloria }
    \new Voice = "bass" { \voiceTwo \bass \bgloria }
    \new Voice { \dynamicUp \tbControl \tbgloriaControl }
  >>
  \new Lyrics = "tb-lyrics" \lyricsto "bass" { \tbVerse \tbgloriaVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Acc."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } { \right \rgloria }
  \new Dynamics { \pnControl \pngloriaControl  }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left \lgloria }
>>
