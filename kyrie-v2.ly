<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a4 g a2 \bar"||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      Let us pray.
    }
  >>
>>
