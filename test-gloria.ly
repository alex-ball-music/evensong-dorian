\version "2.18.2"
\language "english"

\header {
  title = "Gloria"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \dorian
  \numericTimeSignature
  \time 3/4
  \tempo 4=120
}

soprano = \relative c'' {
  \global
  % Music follows here.
  R2.*4
  a2 a4 b a g
  a2 f4 g f e
  d2 r4 R2.
  a'4 a f g( f) e
  d2 r4
  r4 d4 e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  r4 a f b c d
  d2. R2.
  r4 a f b c d
  d2. r2 a4
  d4 r2 \bar"|."
}

alto = \relative c' {
  \global
  % Music follows here.
  R2.*4
  f2 f4 g f e
  f2 d4 g f e
  d2 r4 R2.
  a'4 a f g( f) e
  d2 r4
  r4 d4 e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  R2.*2
  r4 a d, g f e
  d2. R2.
  r4 a' d, g f e
  d4 r2
}

tenor = \relative c' {
  \global
  % Music follows here.
  R2.*4
  a2 a4 b c b
  a2 f4 g f e
  d4 a' c r b d
  c b a g( f) e
  d2 r4 r4 d e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  r4 a f g a b
  a2. R2.
  r4 a f g a b
  a2. r2 a4
  a4 r2
}

bass = \relative c {
  \global
  % Music follows here.
  R2.*4
  d2 d4 g g g
  f2 a4 g f e
  d4 f a r g b
  a g f g( f) e
  d2 r4 r4 d4 e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  R2.*2
  r4 a d, g f e
  d2. R2.
  r4 a' d, g f e
  d4 r2
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Glo -- ry be to the Fa -- ther,
  and to the Son,
  and to the Ho -- ly Ghost;
  as it was in the be -- gin -- ning,
  is now and e -- ver shall be,
  world with -- out end. A -- men.
  World with -- out end. A -- men. A -- men.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  Glo -- ry be to the Fa -- ther,
  and to the Son,
  Glo -- ry,
  Glo -- ry be to the Ho -- ly Ghost;
  as it was in the be -- gin -- ning,
  is now and e -- ver shall be,
  world with -- out end. A -- men.
  World with -- out end. A -- men.
}

right = \relative c' {
  \global
  % Music follows here.
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <f a>8 d <f a> d <g e>4 <f d> <e a>
  r4 <f a>8 d <f a> d <g b>4 <a f> <g e>
  r4 <f a>8 d <f a> d g4 f <e a>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <f a>8 d <f a> d <g e>4 <f d> <e c>
  r4 <f a>8 d <f a> d <b' d>4 <a c> <g b>
  r4 <f a>8 d <f a> d <g b>4 <a f> <g e>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <f a>8 d <f a> d <g b>4 <f a> <e g>
  a4 <e a>8 d <e a>8 d <cs a'>4 <d a'> <e a>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <a f'>8 d <a f'> d <g, e'>4 <f d'> <e c'>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <a f'>8 d <a f'> d <g, e'>4 <f d'> <e c'>
  <d' a>4 r2
}

left = \relative c, {
  \global
  % Music follows here.
  <d d'>2. g2.
  <d d'>2. c2.
  <d d'>2. g2.
  <d d'>2. c2.
  <d d'>2. g2.
  <d d'>2. c2.
  <d d'>2. g2.
  <d d'>2. c2.
  <d d'>2. g2.
  <d d'>2. g2.
  a2. a2.
  <d, d'>2. g2.
  <d d'>2. <c c'>2.
  <d d'>2. g2.
  <d d'>2. <c c'>2.
  <d d'>4 r2
}

choirPart = \new ChoirStaff <<
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
  >>
  \new Lyrics \with {
    alignAboveContext = "sa"
    \override VerticalAxisGroup #'staff-affinity = #DOWN
  } \lyricsto "soprano" \sopranoVerse
  \new Lyrics \lyricsto "alto" \altoVerse
  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
  >>
  \new Lyrics \with {
    alignAboveContext = "tb"
    \override VerticalAxisGroup #'staff-affinity = #DOWN
  } \lyricsto "tenor" \tenorVerse
  \new Lyrics \lyricsto "bass" \bassVerse
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "acoustic grand"
  } \right
  \new Staff = "left" \with {
    midiInstrument = "acoustic grand"
  } { \clef bass \left }
>>

\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
  \midi { }
}
