\version "2.24.0"

#(set-global-staff-size 19)

\paper {
  #(define fonts
    (make-pango-font-tree "Source Serif Pro"
                          "Source Sans Pro"
                          "Source Code Pro"
                          (/ staff-height pt 20)))

  #(set-paper-size "a4")
  bookTitleMarkup = \markup {
    \override #'(baseline-skip . 3.5)
    \large \column {
      \override #'(baseline-skip . 3.5)
      \column {
        \fill-line {
          \italic \fromproperty #'header:dedication
        }
        \vspace #0.5
        \fontsize #2 \larger \bold
        \fill-line {
          \larger \sans \fromproperty #'header:title
        }
        \fill-line {
          \fontsize #1 \smaller \bold
          \larger \sans \fromproperty #'header:subtitle
        }
        \fill-line {
          \smaller \sans \bold
          \fromproperty #'header:subsubtitle
        }
        \fill-line {
          \fromproperty #'header:poet
          { \fontsize #1 \bold \fromproperty #'header:instrument }
          \fromproperty #'header:composer
        }
        \fill-line {
          \fromproperty #'header:meter
          \fromproperty #'header:arranger
        }
      }
    }
  }
  scoreTitleMarkup = \markup {
    \column {
      \if \should-print-all-headers { \bookTitleMarkup \hspace #1 }
      \fill-line {
        { \fontsize #2 \fromproperty #'header:piece }
        \fromproperty #'header:opus
      }
    }
  }
  oddHeaderMarkup = \markup {
    \fill-line {
      \unless \on-first-page \fromproperty #'header:instrument
    }
  }
  evenHeaderMarkup = \oddHeaderMarkup
  oddFooterMarkup = \markup {
    \vspace #1
    \column {
      \fill-line {
        %% Copyright header field only on first page.
        \if \on-first-page \sans \fromproperty #'header:copyright
      }
      \if \on-first-page { \if \on-last-page \vspace #1 }
      \fill-line {
        %% Tagline header field only on last page.
        \if \on-last-page \sans \fromproperty #'header:tagline
      }
      \fill-line {
        %% Page numbers on all pages (if turned on) except the last
        \if \should-print-page-number
        \on-the-fly #not-last-page \sans \fromproperty #'page:page-number-string
      }
      \fill-line {
        %% Page number (if turned on) on last page iff there's no tagline
        \if \should-print-page-number
        \if \on-last-page
        \on-the-fly #no-tagline
        \sans \fromproperty #'page:page-number-string
      }
    }
  }
  ragged-bottom = ##f
  top-margin = 10
  left-margin = 15
  right-margin = 15
  bottom-margin = 10
  last-bottom-spacing.basic-distance = #0
  markup-system-spacing.padding = #2
}

\layout {
  \context { \Score
    \override RehearsalMark.font-size = #1.5
  }
  \context { \Voice
    \override DynamicTextSpanner.font-size = #0
  }
  \context { \Lyrics
    \override StanzaNumber.font-size = #0
    \override LyricText.font-size = #0
  }
  \context { \Dynamics
    \override DynamicTextSpanner.font-size = #0
  }
}
