# A Dorian Setting for Choral Evensong

The cathedral and college chapel choirs of the UK sing the service of
Evensong from the Book of Common Prayer on a daily basis. To serve that need,
there are more musical settings of that service than you can shake a proverbial
stick at, and not much musical ground left to cover other than what one might
politely describe as a challenging listen.

It was therefore with some surprise that I found myself writing a setting of my
own. I had been singing a lot of modern plainchant and found myself imagining
how Evensong might sound if the normal three-note melodies were replaced with
something more tuneful, yet still modal. It all kind of snowballed from there.

One thing I was very clear about was that it should be usable in a parish
context. While the aforementioned cathedrals are spoilt for choice, most of
those settings are too hard for a parish choir to learn in a reasonable amount
of time, or else they depend on having trebles who can hit top Gs and As. I
wanted my setting to be easy to learn, easy to sing and with a comfortable
ambitus.

For the responses, I could have just left them as pseudo-Gregorian melodies,
but for a challenge I decided to have a go at four-part plainchant. After all,
it's not something you hear every day. I went right out of my comfort zone with
some modern-sounding (yet still modal) harmonies, attempting to get some nice
effects with a minimum of movement in the lower parts to keep it simple.

For the canticles, I wanted to go the other way and make them a bit more ‘soft
rock’. As a result, they are much more chord-based with strong rhythm, but to
keep them interesting I play around with time signatures a bit.

## Summary information

### Responses

  - *Voicing:* SATB or unison voices

  - *Notes on ambitus:* Soprano goes up to D; Tenor to F sharp.

  - *Instrumentation:* May be sung *a capella* or doubled on organ/piano

### Canticles

  - *Voicing:* SATB

  - *Notes on ambitus:* Soprano goes up to F but may be easily adapted to go
    only up to D; Tenor goes up to G but with an option to go only up to F.

  - *Instrumentation:* Organ or Piano

  - *Approximate performance length:* Magnificat – 3:20; Nunc Dimittis – 1:50

### General

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) and
[LaTeX](https://www.ctan.org) source code.

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/evensong-dorian/-/releases).
