global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 a c2 b4 c b a a \bar"'"
  a d a a b c c \bar","
  b2 c4 b a \bar"'" g2 a \bar "||"
}

alto = \relative c' {
  \global
  e4 e f2 g4 g g e e
  e fs fs fs fs g g
  g2 g4 g e e2 e
}

tenor = \relative c' {
  \global
  c4 c c2 d4 d d d c
  c d d d d fs e
  d2 e4 e d4 d2 c
}

bass = \relative c' {
  \global
  a4 a a2 a4 a a a a
  a a a a a c c
  b2 a4 g e e2 a,
}

verse = \lyricmode {
  \all As it was in the be -- gin -- ning is now and e -- ver shall be; world with -- out end. A -- men.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
