global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'2( b4 g) f1 \bar "||"
}

alto = \relative c' {
  \global
  e2( f4 d) d1
}

tenor = \relative c' {
  \global
  c1 c
}

bass = \relative c' {
  \global
  a1 a
}

verse = \lyricmode {
  \all A -- men.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo  \bass }
  >>
>>
