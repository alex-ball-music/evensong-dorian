global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 d a a c b( g) a2 \bar "||"
}

alto = \relative c' {
  \global
  e4 g e \accidentalStyle modern-voice f f f2 e2
}

tenor = \relative c' {
  \global
  c4 b c c c d2 d2
}

bass = \relative c' {
  \global
  a4 g g \accidentalStyle modern-voice f f g2 a2
}

verse = \lyricmode {
  \all And grant us thy sal -- va -- tion.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
