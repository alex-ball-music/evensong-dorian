<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a4 a( g) a b( c) a2 \bar "||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      The Lord be with you.
    }
  >>
>>
