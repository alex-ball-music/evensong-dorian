sagloriaControl = {
  s2.*9 s2.\f
  s2.*8 s2.\ff
  s2.*8
  \bar "|."
}

sgloria = \relative c'' {
  % Music follows here.
  a2 a4 b a g
  a2 f4 g f e
  d2 r4 R2.
  a'4 a f g( f) e
  d2 r4
  r4 d4 e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  r4 a f b c d
  d2. R2.
  r4 a f b c d
  d4 f4 d e d a4
  d4 r2
}

agloria = \relative c' {
  % Music follows here.
  f2 f4 g f e
  f2 d4 g f e
  d2 r4 R2.
  a'4 a f g( f) e
  d2 r4
  r4 d4 e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  r4 a f g a b
  a2. R2.
  r4 a f g a b
  a4 d a c a a4
  a4 r2
}

sagloriaVerse = \lyricmode {
  Glo -- ry be to the Fa -- ther,
  and to the Son,
  and to the Ho -- ly Ghost;
  as it was in the be -- gin -- ning,
  is now and e -- ver shall be,
  world with -- out end. A -- men.
  World with -- out end. A -- men.
  World with -- out end. A -- men.
}

tbgloriaControl = {
  s2.*9 s2.\f
  s2.*10 s2.\ff
}

tgloria = \relative c' {
  % Music follows here.
  a2 a4 b c b
  a2 f4 g f e
  d4 a' c r b d
  c b a g( f) e
  d2 r4 r4 <d d'> <e e'>
  <f f'>2. <g g'>4 <f f'> <e e'>
  <d d'> <d d'> r r2 <d d'>4
  a'2 a4 b4 a g
  a2 r4 R2.
  R2.*2
  r4 a <d, d'> <g g'> <f f'> <e e'>
  <d d'>2. R2.
  r4 a' <d, d'> <g g'> <f f'> <e e'>
  <d d'>4 r2
}

bgloria = \relative c {
  % Music follows here.
  d2 d4 g g g
  f2 a4 g f e
  d4 f a r g b
  a g f g( f) e
  d2 r4 r4 d4 e
  f2. g4 f e
  d d r r2 d4
  a'2 a4 b4 a g
  a2 r4 R2.
  R2.*2
  r4 a d, g f e
  d2. R2.
  r4 a' d, g f e
  d4 r2
}

tbgloriaVerse = \lyricmode {
  Glo -- ry be to the Fa -- ther,
  and to the Son,
  Glo -- ry,
  Glo -- ry be to the Ho -- ly Ghost;
  as it was in the be -- gin -- ning,
  is now and e -- ver shall be,
  world with -- out end. A -- men.
  World with -- out end. A -- men.
}

pngloriaControl = {
  s2.*9 s4\> s s
  s2.\f s2.*5
  s2.\< s2. s2.\ff
}

rgloria = \relative c' {
  % Music follows here.
  r4 <f a>8 d <f a> d <g b>4 <a f> <g e>
  r4 <f a>8 d <f a> d <g e>4 <f d> <e c>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <f a>8 d <f a> d <g e>4 <f d> <e c>
  r4 <f a>8 d <f a> d <b' d>4 <a c> <g b>
  r4 <f a>8 d <f a> d <g e>4 <a f> <g e>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <f a>8 d <f a> d <g b>4 <f a> <e g>
  a4 <e a>8 d <e a>8 d <cs a'>4 <d a'> <e a>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <a f'>8 d <a f'> d <g, e'>4 <f d'> <e c'>
  r4 <f a>8 d <f a> d <g b>4 <a c> <b d>
  r4 <a f'>8 d <a f'> d <g, e'>4 <f d'> <e c'>
  <d a' d>4 r2
}

lgloria = \relative c {
  % Music follows here.
  << { r4 a  s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 a  s r g s } \\ { <d d'>2 q4 <c c'>2 q4 } >>
  << { r4 a' s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 a  s r g s } \\ { <d d'>2 q4 <c c'>2 q4 } >>
  << { r4 a' s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 a  s r g s } \\ { <d d'>2 q4 <c c'>2 q4 } >>
  << { r4 a' s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 a  s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 e' s r e s } \\ { <a, a'>2 q4 q2 q4 } >>
  << { r4 a  s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 a  s r g s } \\ { <d d'>2 q4 <c c'>2 q4 } >>
  << { r4 a' s r d s } \\ { <d, d'>2 q4 <g g'>2 q4 } >>
  << { r4 a  s r g s } \\ { <d d'>2 q4 <c c'>2 q4 } >>
  <d a' d>4 r2
}
