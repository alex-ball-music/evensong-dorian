\version "2.20.0"
\include "common.ly"
\include "common-resp.ly"

\score {
  \relative <<
  \new Staff <<
  \clef treble
  \key a \dorian
  \new Voice { \voiceOne
    a'4 a fs g a b d c b( g) a2  \bar "||"}
  \addlyrics { \all And take not thy Ho -- ly Spi -- rit from us. }
  \new Voice { \voiceTwo \override NoteHead.style = #'diamond
    e4 d d d d d e e e2 fs2 }
  >>
  \new Staff <<
  \clef bass
  \key a \dorian
  { \voiceOne
    c4 a a a a b c c c( d) d2 } \\
  { \voiceTwo \override NoteHead.style = #'diamond
    a4 fs d e fs g g g g( e) d }
  >>
  >>
  \include "common-greg.ly"
}

\markup { \bold {Collects} }

\score {
  \relative c'' <<
  \new Staff <<
  \clef treble
  \key a \dorian
  \new Voice {
    a1 g4 a2 \bar"||"
  }
  \addlyrics {
    ". . . for" e -- ver.
  }
  >>
  >>
  \include "common-greg.ly"
  \midi{ \tempo 4=120 }
}

\score {
  \relative c'' <<
  \new Staff <<
  \clef treble
  \key a \dorian
  \new Voice {
    a1 g4 a2 \bar"||"
  }
  \addlyrics {
    ". . . our" Sav -- iour.
  }
  >>
  >>
  \include "common-greg.ly"
  \midi{ \tempo 4=120 }
}

\score {
\relative c'' <<
\new Staff <<
\clef treble
\key a \dorian
\new Voice {
  a1 g4 a2 \bar"||"
  \stopStaff s2 \startStaff
  s1 a1 g4 a2 \bar"||"
  \stopStaff s2 \startStaff
  s1 a1 g4 a2 \bar"||"
}
\addlyrics {
  ". . . for" e -- ver.
  ". . . our" Sav -- iour.
  ". . . Je" -- sus Christ.
}
>>
>>
\include "common-greg.ly"
}
