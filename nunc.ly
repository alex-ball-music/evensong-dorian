global = {
  \key d \dorian
  \time 4/4
  \tempo 4=120
}

saControl = {
  s1*5 s2 s2
  s1\p s1*3
  s1 s2 s2\p
  s1 s1
  s1 s2 s2
  s1 s1
  s1 s1\<
  s1\mp s1
  \time 3/4
  s4\< s s s\> s s\!
  s2.
  \bar "||" \mark\default
  s4\< s s
  s2.\mf s2.
  s2. s4 s\< s
  s2.\f s2.
  s2.\< s
  s\ff s
  s s
  \bar "||" \mark\default
}

tbControl = {
  s1*5 s2 s2
  s1\p s1*3
  s1 s2 s2\p
  s1 s1
  s1 s2 s2
  s1 s1
  s1 s1\<
  s1\mp s1
  \time 3/4
  s4\< s s s\> s s\!
  s2.

  s4\< s s
  s2.\mf s2.
  s2. s4 s\< s
  s2.\f s2.
  s2.\< s
  s s\ff
  s s
}

pnControl = {
  s4\mp s2. s1*4 s4\> s2 s4\!
  s1\p s1*3
  s1 s2 s2
  s1 s1
  s1 s2 s2
  s1 s1
  s1 s1\<
  s1\mp s1
  \time 3/4
  s4 s s s s s
  s2.

  s4\< s s
  s2.\mf s2.
  s2. s4 s\< s
  s2.\f s2.
  s2.\< s
  s s
  s\ff s
}


soprano = \relative c' {
  \global
  % Music follows here.
  R1*6 \oneVoice
  s1*5
  r2^\markup { \halign #0.2 \smcp soprano } d a' a a g a1
  s1*4 \voiceOne
  d,4 e f g
  a2 bf g f
  \time 3/4
  a4 bf a g( f e)
  d2 r4 a'4 a b
  c2 d4 b4 a g
  a a2 r4 a8 g a b
  c4. b8 c d b2 g4
  a2. d
  cs2. ~ cs2.
  R2.*2
}

alto = \relative c' {
  \global
  % Music follows here.
  R1*5 R1^\markup { \smcp alto } \oneVoice
  d2 f e4 d e c
  d2 f4 f e2 c
  d1
  s1*4
  r2^\markup { \smcp alto } d4 e
  f2 f4( g4) e2 d4 c
  d2 d4 r^\markup { \halign #0 \smcp tutti } \voiceTwo d4 c d e
  f2 g e d
  \time 3/4
  f4 g f e( d c)
  d2 r4 a'4 a a
  a2 b4 g f g
  a4 a2 r4 a8 g a a
  a4. g8 a b g2 g4
  f2. a2.
  a2. ~ a2.
  R2.*2
}

tenor = \relative c {
  \global
  % Music follows here.
  R1*6 \oneVoice
  s1*5
  r2^\markup { \halign #0 \smcp tenor } d a' a a g a1
  s1*4 \voiceOne
  d,4 e f g
  a2 bf g f
  \time 3/4
  a4 bf a g( f e)
  d2 r4
  d4 f a c2 b4
  d4 d b cs cs2
  r4 c!8 c c d c4. b8 c d
  d2 d4 r4 d f
  r d f r e e e2.
  R2.*2
}

bass = \relative c {
  \global
  % Music follows here.
  R1*5 R1^\markup { \smcp bass } \oneVoice
  d2 f e4 d e c
  d2 f4 f e2 c
  d1
  s1*4
  r2^\markup { \smcp bass } d4 e
  f2 f4( g) e2 d4 c
  d2 d4 r^\markup { \halign #0 \smcp tutti } \voiceTwo d4 c d e
  f2 g e d
  \time 3/4
  f4 g f e( d c)
  d2 r4
  d4 d d f2 f4
  g4 g g a4 a2
  r4 a8 a a g f4. f8 f f
  g2 g4 r4 a d
  r a d r cs cs cs2.
  R2.*2
}

saVerse = \lyricmode {
  % Lyrics follow here.
  Lord, now let -- test thou thy ser -- vant de -- part in peace
  For mine eyes have seen
  thy sal -- va -- tion;
  Which thou hast pre -- pared
  be -- fore the face of all peo -- ple;
  To be a light to light -- en the Gen -- tiles,
  and to be the glo -- ry of thy peo -- ple Is -- ra -- el.
}

stVerse = \lyricmode {
  % Lyrics follow here.
  ac -- cord -- ing to thy word.
}


tbVerse = \lyricmode {
  % Lyrics follow here.
  \repeat unfold 54 \sh
  Glo -- ry, glo -- ry, Is -- ra -- el.
}

right = \relative c'' {
  \global
  % Music follows here.
  R1*2
  d2 f e4 d e c
  d2 a g4 f e c
  << { r2 <f a>4. r8 } \\ { r4 d2 d4 } >> <e g>4 <d f> <e g> <c e>
  << { r2 <f a>4. r8 } \\ { r4 d2 d4 } >> <e g>4 c <e g> c
  << { r2 <f a>4. r8 r2 <g b>4. r8 } \\ { r4 d2 d4 r4 d2 d4 } >>
  << { r2 <f a>4. r8 <f a>4. r8 <f g>4. r8 } \\ { r4 d2 d4 ~ d4 c2 c4 } >>
  << { r2 <e a>4. r8 r2 <e a>4. r8 } \\ { r4 d2 d4 r4 cs d cs } >>
  <d f>4 a <c f> a << { r2 <e' g>4. r8 } \\ { r4 c2 c4 } >>
  << { r2 <g' b>4. r8 r2 <f a>4. r8 } \\ { r4 d2 d4 r4 d2 d4 } >>
  <f a>4 c <g' bf> c, <e g> c <f d> c
  <f a> <g bf> <f a> <e g> <d f> <c e>
  << { r4 <f a>4. r8 r4 <f a>4. r8 } \\ { d2 d4 d2 d4 } >>
  << { r4 <a' c> r } \\ { f2 f4 } >> <g b>4 <f a> <e g>
  << { r4 <a cs>4. r8 r4 <a c!>4. r8 } \\ { e2 e4 e2 e4 } >>
  << { r4 <a c>4. r8 r4 <b d> r } \\ { f2 f4 g2 g4 } >>
  << { r4 <f a> <a d> r4 <f a> <a d> } \\ { d,2. d2. } >>
  e4 <a cs>8 e <a cs>4 e <a cs>8 e <a cs>4
  e <a cs>4 <a cs>4 <e a cs>2.
}

left = \relative c' {
  \global
  % Music follows here.
  << {
    a2\rest <f a>2
    a2\rest <g b>2
    a2\rest <f a>2
    a2\rest <g b>2
    a2\rest <f a>2
  } \\ {
    d,1_\markup{ \italic "ped." }
    g1
    d1
    g1
    d1
  } \\ { \stemDown
    d'4\rest d2.
    d4\rest d2.
    d4\rest d2.
    d4\rest d2.
    d4\rest d2.
  } >> c2_\markup{ \italic "man." } c
  d1 g,1
  d'1 g,1
  d'1 g,1
  d'2 d, f2 f2
  a1 a1
  d2 f2 c1
  g1 d'1
  f2 f c c
  bf2. c2.
  d2. d,2.
  f2. g2.
  a2. a2 g4
  f2. g2.
  a2. f2.
  <a a'>2 <bf bf'>4 <a a'>2 <g g'>4
  <a a'>2. <a a'>4 <g g'> <e e'>
}

choirPart = \new ChoirStaff <<
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano" { \voiceOne \soprano \sgloria }
    \new Voice = "alto" { \voiceTwo \alto \agloria }
    \new Voice { \dynamicUp \saControl \sagloriaControl }
  >>
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \stVerse }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \saVerse \sagloriaVerse }
  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor \tgloria }
    \new Voice = "bass" { \voiceTwo \bass \bgloria }
    \new Voice { \dynamicUp \tbControl \tbgloriaControl }
  >>
  \new Lyrics = "tb-lyrics" \lyricsto "bass" { \tbVerse \tbgloriaVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Acc."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } { \right \rgloria }
  \new Dynamics { \pnControl \pngloriaControl  }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left \lgloria }
>>
