global = {
  \key d \dorian
  \time 4/4
}

soprano = \relative c' {
  \global
  d1 f2 g a1 \bar"||"
  g1 c2 b a g a1 \bar"||"
  a1 b2 g a1 \bar"||"
  c1 d2 a f e d1 \bar"||"
}

alto = \relative c' {
  \global
  d1 d2 e f1
  e1 f2 g e e e1
  f1 g2 e f1
  g1 a2 f d c d1
}

tenor = \relative c {
  \global
  f1 a2 c c1
  c1 c2 d c b cs1
  d1 d2 d d1
  e1 f2 c a2. g4 fs?1
}

bass = \relative c {
  \global
  d1 d2 c f1
  c1 a2 d e e a1
  a1 g2 g f1
  e1 d2 f a a, d1
}

verse = \lyricmode {
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
  >>
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
  >>
>>
