\version "2.20.0"
\include "common.ly"
\include "common-resp.ly"

\header {
  title = "The Lord’s Prayer"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\include "lords-prayer-end-long.ly"
\include "lords-prayer.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}
