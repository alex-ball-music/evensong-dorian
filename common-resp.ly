\version "2.24.0"

\layout {
  indent = #0
  short-indent = #0
  \context { \Score
    \remove "Bar_number_engraver"
    \override RehearsalMark.font-size = #2
  }
  \context {
    \Staff
    \remove Time_signature_engraver
  }
  \context { \Voice
    \override DynamicTextSpanner.font-size = #2.6
  }
  \context { \Lyrics
    \override StanzaNumber.font-size = #2.6
    \override LyricText.font-size = #2.6
  }
  \context { \Dynamics
    \override DynamicTextSpanner.font-size = #2.6
  }
}
