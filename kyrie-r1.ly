global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 g a b( c) a2 \bar "||"
}

alto = \relative c' {
  \global
  e4 e fs g( e) e2
}

tenor = \relative c' {
  \global
  c4 d d d2 c
}

bass = \relative c' {
  \global
  a4 b a e( g) a2
}

verse = \lyricmode {
  \all And with thy spi -- rit.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
