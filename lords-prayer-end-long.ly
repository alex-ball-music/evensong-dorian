lpEndS = \relative c' {
  e4 g a a b a \bar"'" b c c b c d d \bar"'"
  d4 c( b) a g b( c) a2 \bar "," c4( b) a( g a2) \bar"||"
}

lpEndA = \relative c' {
  e4 e fs fs gs fs g g g g g a a
  g4 g2 e4 e e2 fs2 g2 fs4( e e2)
}

lpEndT = \relative c' {
  b4 b c d e e e e e d e e fs
  d4 e( d) c b c2 d2 e2 b2( cs2)
}

lpEndB = \relative c {
  e4 g fs fs e e b' c c c g d' d
  b4 c( g) a e g2 a2 a4( g) e2( a2)
}

lpEndL = \lyricmode {
  For thine is the king -- dom,
  the pow -- er and the glo -- ry,
  for e -- ver and e -- ver.
  A -- men.
}