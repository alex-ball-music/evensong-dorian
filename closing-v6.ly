<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c' {
        e4 g2 a4 b( a) g a2 a4 b( c) a2 \bar "||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      O God, make clean our hearts with -- in us.
    }
  >>
>>
