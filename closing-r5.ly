global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 a g a b c b c d c b a2 \bar"'"
  a4 c c b \bar"'" g a2 \bar "||"
}

alto = \relative c' {
  \global
  e4 fs e fs g a g a b a g g2
  fs4 a \accidentalStyle modern-voice f f e e2
}

tenor = \relative c' {
  \global
  c4 d d d d e e e e e e d2
  d4 c c d d cs2
}

bass = \relative c' {
  \global
  a4 a a a a a a a gs a b b2
  a4 f a g? b a2
}

verse = \lyricmode {
  \all Be -- cause there is none o -- ther that fight -- eth for us,
  but on -- ly thou, O Lord.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
