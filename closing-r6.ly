global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 b g a b c d c b( g) a2  \bar "||"
}

alto = \relative c' {
  \global
  e4 g e fs g a a a e2 e2
}

tenor = \relative c' {
  \global
  c4 d d d d e d d g,( b) cs2
}

bass = \relative c' {
  \global
  a4 a b a g g fs fs e2 a
}

verse = \lyricmode {
  \all And take not thy Ho -- ly Spi -- rit from us.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
