<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c' {
        e4 g2 a4 b( a) g a2 a \bar "||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      O God, make speed to save us.
    }
  >>
>>
