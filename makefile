SHELL=bash
PWD   = $(shell pwd)
TEMP := $(shell mktemp -d -t tmp.XXXXXXXXXX)
OUTDIR=releases/$(shell date +%F)
AUDIO=closing-prayers-preces magnificat nunc-dimittis lords-prayer-full opening-preces psalm-anglican-chant
OPENING=common-greg.ly common.ly common-resp.ly opening-r1.ly opening-r2.ly opening-r3.ly opening-r4.ly opening-v1.ly opening-v2.ly opening-v3.ly opening-v4.ly
CLOSING=amen-1.ly amen-2.ly amen-3.ly closing-r1.ly closing-r2.ly closing-r3.ly closing-r4.ly closing-r5.ly closing-r6.ly closing-v1.ly closing-v2.ly closing-v3.ly closing-v4.ly closing-v5.ly closing-v6.ly common-greg.ly common.ly common-resp.ly kyrie-r1.ly kyrie-r2.ly kyrie-v1.ly kyrie-v2.ly lords-prayer.ly
CHANT=psalm-anglican.ly
REHEARSEMP3 := $(AUDIO:%=%-soprano.mp3) $(AUDIO:%=%-alto.mp3) $(AUDIO:%=%-tenor.mp3) $(AUDIO:%=%-bass.mp3)
PARTS=soprano alto tenor bass

ifeq ($(OS),Windows_NT)
	SFFLAG=
else
	UNAME := $(shell uname)
	ifeq ($(UNAME),Darwin)
		SFFLAG=-s /Library/Audio/Sounds/Banks/FluidR3_GM.sf2
	endif
	ifeq ($(UNAME),Linux)
		SFFLAG=-s /usr/share/sounds/sf2/FluidR3_GM.sf2
	endif
endif

.PHONY: clean release
.INTERMEDIATE: opening-1.midi opening-1-soprano.midi opening-1-alto.midi opening-1-tenor.midi opening-1-bass.midi opening-2.midi opening-2-soprano.midi opening-2-alto.midi opening-2-tenor.midi opening-2-bass.midi opening-3.midi opening-3-soprano.midi opening-3-alto.midi opening-3-tenor.midi opening-3-bass.midi opening-4.midi opening-4-soprano.midi opening-4-alto.midi opening-4-tenor.midi opening-4-bass.midi kyrie-1.midi kyrie-1-soprano.midi kyrie-1-alto.midi kyrie-1-tenor.midi kyrie-1-bass.midi kyrie-2.midi kyrie-2-soprano.midi kyrie-2-alto.midi kyrie-2-tenor.midi kyrie-2-bass.midi closing-1.midi closing-1-soprano.midi closing-1-alto.midi closing-1-tenor.midi closing-1-bass.midi closing-2.midi closing-2-soprano.midi closing-2-alto.midi closing-2-tenor.midi closing-2-bass.midi closing-3.midi closing-3-soprano.midi closing-3-alto.midi closing-3-tenor.midi closing-3-bass.midi closing-4.midi closing-4-soprano.midi closing-4-alto.midi closing-4-tenor.midi closing-4-bass.midi closing-5.midi closing-5-soprano.midi closing-5-alto.midi closing-5-tenor.midi closing-5-bass.midi closing-6.midi closing-6-soprano.midi closing-6-alto.midi closing-6-tenor.midi closing-6-bass.midi closing-7.midi closing-7-soprano.midi closing-7-alto.midi closing-7-tenor.midi closing-7-bass.midi closing-8.midi closing-8-soprano.midi closing-8-alto.midi closing-8-tenor.midi closing-8-bass.midi closing-9.midi closing-9-soprano.midi closing-9-alto.midi closing-9-tenor.midi closing-9-bass.midi

all: dorian-canticles.pdf dorian-responses.pdf

release: $(OUTDIR)/dorian-canticles.pdf $(OUTDIR)/dorian-responses.pdf $(OUTDIR)/dorian-responses-booklet.pdf $(AUDIO:%=$(OUTDIR)/%.mp3) $(PARTS:%=$(OUTDIR)/dorian-canticles_rehearsal-%.zip) $(PARTS:%=$(OUTDIR)/dorian-responses_rehearsal-%.zip) $(OUTDIR)/dorian-evensong_midi.zip

$(OUTDIR):
	mkdir -p $(OUTDIR)

$(OUTDIR)/dorian-canticles.pdf: dorian-canticles.pdf $(OUTDIR)
	cp $< $(OUTDIR)

$(OUTDIR)/dorian-responses.pdf: dorian-responses.pdf $(OUTDIR)
	cp $< $(OUTDIR)

$(OUTDIR)/dorian-responses-booklet.pdf: dorian-responses.pdf $(OUTDIR)
	pdfjam --booklet true --landscape --a4paper -o $@ -- $<

dorian-canticles.pdf: dorian-canticles.tex book-mag.ly book-nunc.ly common.ly common-cant.ly mag.ly nunc.ly gloria.ly
	latexmk -pdflua -quiet --shell-escape --interaction=nonstopmode $<

dorian-responses.pdf: dorian-responses.tex $(CLOSING) lords-prayer-end-both.ly
	latexmk -pdflua -quiet --shell-escape --interaction=nonstopmode $<

$(OUTDIR)/magnificat.mp3: test-mag.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

test-mag.midi test-mag-alto.midi test-mag-bass.midi test-mag-soprano.midi test-mag-tenor.midi: test-mag.ly common.ly common-test.ly gloria.ly mag.ly
	lilypond $<

$(OUTDIR)/nunc-dimittis.mp3: test-nunc.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

test-nunc.midi test-nunc-alto.midi test-nunc-bass.midi test-nunc-soprano.midi test-nunc-tenor.midi: test-nunc.ly common.ly common-test.ly gloria.ly nunc.ly
	lilypond $<

$(OUTDIR)/lords-prayer-full.mp3 $(OUTDIR)/psalm-anglican-chant.mp3: $(OUTDIR)/%.mp3: %.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

lords-prayer-full.midi: lords-prayer-full.ly lords-prayer-end-long.ly
	lilypond $<

psalm-anglican-chant.midi: psalm-anglican-chant.ly psalm-anglican.ly
	lilypond $<

$(OUTDIR)/opening-preces.mp3: opening-1.midi opening-2.midi opening-3.midi opening-4.midi
	./midi2mp3.py $(SFFLAG) -o $@ -g 2 $^

opening-1.midi: opening-preces-0.midi opening-preces-1.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-2.midi: opening-preces-2.midi opening-preces-3.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-3.midi: opening-preces-4.midi opening-preces-5.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-4.midi: opening-preces-6.midi opening-preces-7.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-1-%.midi: opening-preces-0.midi opening-preces-1-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-2-%.midi: opening-preces-2.midi opening-preces-3-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-3-%.midi: opening-preces-4.midi opening-preces-5-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-4-%.midi: opening-preces-6.midi opening-preces-7-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

opening-preces-0.midi opening-preces-1-alto.midi opening-preces-1-bass.midi opening-preces-1.midi opening-preces-1-soprano.midi opening-preces-1-tenor.midi opening-preces-2.midi opening-preces-3-alto.midi opening-preces-3-bass.midi opening-preces-3.midi opening-preces-3-soprano.midi opening-preces-3-tenor.midi opening-preces-4.midi opening-preces-5-alto.midi opening-preces-5-bass.midi opening-preces-5.midi opening-preces-5-soprano.midi opening-preces-5-tenor.midi opening-preces-6.midi opening-preces-7-alto.midi opening-preces-7-bass.midi opening-preces-7.midi opening-preces-7-soprano.midi opening-preces-7-tenor.midi: opening-preces.ly $(OPENING)
	lilypond $<

$(OUTDIR)/closing-prayers-preces.mp3: kyrie-1.midi kyrie-2.midi closing-prayers-preces-04.midi closing-1.midi closing-2.midi closing-3.midi closing-4.midi closing-5.midi closing-6.midi closing-7.midi closing-8.midi closing-9.midi
	./midi2mp3.py $(SFFLAG) -o $@ -g 2 $^

kyrie-1.midi: closing-prayers-preces-00.midi closing-prayers-preces-01.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

kyrie-2.midi: closing-prayers-preces-02.midi closing-prayers-preces-03.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-1.midi: closing-prayers-preces-05.midi closing-prayers-preces-06.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-2.midi: closing-prayers-preces-07.midi closing-prayers-preces-08.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-3.midi: closing-prayers-preces-09.midi closing-prayers-preces-10.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-4.midi: closing-prayers-preces-11.midi closing-prayers-preces-12.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-5.midi: closing-prayers-preces-13.midi closing-prayers-preces-14.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-6.midi: closing-prayers-preces-15.midi closing-prayers-preces-16.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-7.midi: closing-prayers-preces-17.midi closing-prayers-preces-18.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-8.midi: closing-prayers-preces-19.midi closing-prayers-preces-20.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-9.midi: closing-prayers-preces-21.midi closing-prayers-preces-22.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

kyrie-1-%.midi: closing-prayers-preces-00.midi closing-prayers-preces-01-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

kyrie-2-%.midi: closing-prayers-preces-02.midi closing-prayers-preces-03-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-1-%.midi: closing-prayers-preces-05.midi closing-prayers-preces-06-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-2-%.midi: closing-prayers-preces-07.midi closing-prayers-preces-08-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-3-%.midi: closing-prayers-preces-09.midi closing-prayers-preces-10-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-4-%.midi: closing-prayers-preces-11.midi closing-prayers-preces-12-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-5-%.midi: closing-prayers-preces-13.midi closing-prayers-preces-14-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-6-%.midi: closing-prayers-preces-15.midi closing-prayers-preces-16-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-7-%.midi: closing-prayers-preces-17.midi closing-prayers-preces-18-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-8-%.midi: closing-prayers-preces-19.midi closing-prayers-preces-20-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-9-%.midi: closing-prayers-preces-21.midi closing-prayers-preces-22-%.midi
	./midi2mp3.py $(SFFLAG) -m $@ -g 1 -t 0 $^

closing-prayers-preces-00.midi closing-prayers-preces-01-alto.midi closing-prayers-preces-01-bass.midi closing-prayers-preces-01.midi closing-prayers-preces-01-soprano.midi closing-prayers-preces-01-tenor.midi closing-prayers-preces-02.midi closing-prayers-preces-03-alto.midi closing-prayers-preces-03-bass.midi closing-prayers-preces-03.midi closing-prayers-preces-03-soprano.midi closing-prayers-preces-03-tenor.midi closing-prayers-preces-04-alto.midi closing-prayers-preces-04-bass.midi closing-prayers-preces-04.midi closing-prayers-preces-04-soprano.midi closing-prayers-preces-04-tenor.midi closing-prayers-preces-05.midi closing-prayers-preces-06-alto.midi closing-prayers-preces-06-bass.midi closing-prayers-preces-06.midi closing-prayers-preces-06-soprano.midi closing-prayers-preces-06-tenor.midi closing-prayers-preces-07.midi closing-prayers-preces-08-alto.midi closing-prayers-preces-08-bass.midi closing-prayers-preces-08.midi closing-prayers-preces-08-soprano.midi closing-prayers-preces-08-tenor.midi closing-prayers-preces-09.midi closing-prayers-preces-10-alto.midi closing-prayers-preces-10-bass.midi closing-prayers-preces-10.midi closing-prayers-preces-10-soprano.midi closing-prayers-preces-10-tenor.midi closing-prayers-preces-11.midi closing-prayers-preces-12-alto.midi closing-prayers-preces-12-bass.midi closing-prayers-preces-12.midi closing-prayers-preces-12-soprano.midi closing-prayers-preces-12-tenor.midi closing-prayers-preces-13.midi closing-prayers-preces-14-alto.midi closing-prayers-preces-14-bass.midi closing-prayers-preces-14.midi closing-prayers-preces-14-soprano.midi closing-prayers-preces-14-tenor.midi closing-prayers-preces-15.midi closing-prayers-preces-16-alto.midi closing-prayers-preces-16-bass.midi closing-prayers-preces-16.midi closing-prayers-preces-16-soprano.midi closing-prayers-preces-16-tenor.midi closing-prayers-preces-17.midi closing-prayers-preces-18-alto.midi closing-prayers-preces-18-bass.midi closing-prayers-preces-18.midi closing-prayers-preces-18-soprano.midi closing-prayers-preces-18-tenor.midi closing-prayers-preces-19.midi closing-prayers-preces-20-alto.midi closing-prayers-preces-20-bass.midi closing-prayers-preces-20.midi closing-prayers-preces-20-soprano.midi closing-prayers-preces-20-tenor.midi closing-prayers-preces-21.midi closing-prayers-preces-22-alto.midi closing-prayers-preces-22-bass.midi closing-prayers-preces-22.midi closing-prayers-preces-22-soprano.midi closing-prayers-preces-22-tenor.midi: closing-prayers-preces.ly $(CLOSING) lords-prayer-end-short.ly
	lilypond $<

$(OUTDIR)/dorian-canticles_rehearsal-%.zip: magnificat-%.mp3 nunc-dimittis-%.mp3
	mkdir $(TEMP)/dorian-canticles_rehearsal-$*
	cp $^ $(TEMP)/dorian-canticles_rehearsal-$*
	cd $(TEMP); zip -Drq $(PWD)/$@ dorian-canticles_rehearsal-$*

$(OUTDIR)/dorian-responses_rehearsal-%.zip: opening-preces-%.mp3 psalm-anglican-chant-%.mp3 closing-prayers-preces-%.mp3 lords-prayer-full-%.mp3
	mkdir $(TEMP)/dorian-responses_rehearsal-$*
	cp $^ $(TEMP)/dorian-responses_rehearsal-$*
	cd $(TEMP); zip -Drq $(PWD)/$@ dorian-responses_rehearsal-$*

opening-preces-%.mp3: opening-1-%.midi opening-2-%.midi opening-3-%.midi opening-4-%.midi
	./midi2mp3.py $(SFFLAG) -o $@ $^

psalm-anglican-chant-%.mp3: psalm-anglican-chant-%.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

magnificat-%.mp3: test-mag-%.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

nunc-dimittis-%.mp3: test-nunc-%.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

closing-prayers-preces-%.mp3: kyrie-1-%.midi kyrie-2-%.midi closing-prayers-preces-04-%.midi closing-1-%.midi closing-2-%.midi closing-3-%.midi closing-4-%.midi closing-5-%.midi closing-6-%.midi closing-7-%.midi closing-8-%.midi closing-9-%.midi
	./midi2mp3.py $(SFFLAG) -o $@ $^

lords-prayer-full-%.mp3: lords-prayer-full-%.midi
	./midi2mp3.py $(SFFLAG) -o $@ $<

$(OUTDIR)/dorian-evensong_midi.zip:
	mkdir $(TEMP)/midi
	cp opening-preces-0.midi opening-preces-1.midi opening-preces-2.midi opening-preces-3.midi opening-preces-4.midi opening-preces-5.midi opening-preces-6.midi opening-preces-7.midi closing-prayers-preces-00.midi closing-prayers-preces-01.midi closing-prayers-preces-02.midi closing-prayers-preces-03.midi closing-prayers-preces-04.midi closing-prayers-preces-05.midi closing-prayers-preces-06.midi closing-prayers-preces-07.midi closing-prayers-preces-08.midi closing-prayers-preces-09.midi closing-prayers-preces-10.midi closing-prayers-preces-11.midi closing-prayers-preces-12.midi closing-prayers-preces-13.midi closing-prayers-preces-14.midi closing-prayers-preces-15.midi closing-prayers-preces-16.midi closing-prayers-preces-17.midi closing-prayers-preces-18.midi closing-prayers-preces-19.midi closing-prayers-preces-20.midi closing-prayers-preces-21.midi closing-prayers-preces-22.midi lords-prayer-full.midi psalm-anglican-chant.midi  $(TEMP)/midi
	cp test-mag.midi $(TEMP)/midi/magnificat.midi
	cp test-nunc.midi $(TEMP)/midi/nunc-dimittis.midi
	cd $(TEMP); zip -Drq $(PWD)/$@ midi

clean:
	latexmk -c
	rm -rf tmp-ly/
	rm -f dorian-*.pdf
	rm -f test*.midi
	rm -f test*.pdf
	rm -f opening*.midi
	rm -f opening*.pdf
	rm -f closing*.midi
	rm -f closing*.pdf
	rm -f lords-prayer*.midi
	rm -f lords-prayer*.pdf
	rm -f psalm-anglican-chant*.midi
	rm -f psalm-anglican-chant*.pdf
	rm -f *-{alto,bass,soprano,tenor}.{mp3,midi}
