global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'1 ~ a a \bar "||"
}

alto = \relative c' {
  \global
  e1 ~ e e
}

tenor = \relative c' {
  \global
  b1( d2 b) cs1
}

bass = \relative c' {
  \global
  g2.( fs4 e1) a1
}

verse = \lyricmode {
  \all A -- men.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
  >>
>>
