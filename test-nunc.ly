\version "2.20.0"
\include "common.ly"
\include "common-test.ly"

\header {
  title = "Nunc Dimittis"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\include "gloria.ly"
\include "nunc.ly"

\score {
  <<
    \choirPart
    \pianoPart
    \context Staff {
      \set Score.instrumentEqualizer = #choral-instrument-equalizer
    }
  >>
  \layout { }
  \midi { }
}

% Rehearsal MIDI files:
sopranoFull = { \soprano \sgloria }
altoFull = { \alto \agloria }
tenorFull = { \tenor \tgloria }
bassFull = { \bass \bgloria }

vIab = \lyricmode {
  Lord, now let -- test thou thy ser -- vant de -- part in peace
  For mine eyes have seen
}

vIst = \lyricmode {
  ac -- cord -- ing to thy word.
}

vII = \lyricmode {
  Which thou hast pre -- pared
  be -- fore the face of all peo -- ple;
  To be a light to light -- en the Gen -- tiles,
}

vIIIsa = \lyricmode {
  and to be the glo -- ry of thy peo -- ple Is -- ra -- el.
}

vIIItb = \lyricmode {
  and to be the glo -- ry of thy peo -- ple.
  Glo -- ry, glo -- ry, Is -- ra -- el.
}

sopranoLibretto = { \vIst \vII \vIIIsa \sagloriaVerse }
altoLibretto = { \vIab \vII \vIIIsa \sagloriaVerse }
tenorLibretto = { \vIst \vII \vIIItb \tbgloriaVerse }
bassLibretto = { \vIab \vII \vIIItb \tbgloriaVerse }

\book {
  \bookOutputSuffix "soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \sopranoLibretto
    \midi { }
  }
}

\book {
  \bookOutputSuffix "alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \altoLibretto
    \midi { }
  }
}

\book {
  \bookOutputSuffix "tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tenorLibretto
    \midi { }
  }
}

\book {
  \bookOutputSuffix "bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \bassLibretto
    \midi { }
  }
}
