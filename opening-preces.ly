\version "2.20.0"
\include "common.ly"
\include "common-resp.ly"

\header {
  title = "Opening Preces"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

% O Lord, open our lips

\score {
  \include "opening-v1.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "0"
  \score {
    \include "opening-v1.ly"
    \midi{ \tempo 4=120 }
  }
}

% And our mouth...

\include "opening-r1.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "1"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "1-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "1-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "1-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "1-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% O God, make speed...

\score {
  \include "opening-v2.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "2"
  \score {
    \include "opening-v2.ly"
    \midi{ \tempo 4=120 }
  }
}

% O Lord, make haste...

\include "opening-r2.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "3"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "3-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "3-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "3-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "3-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Glory be...

\score {
  \include "opening-v3.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "4"
  \score {
    \include "opening-v3.ly"
    \midi{ \tempo 4=120 }
  }
}

% As it was...

\include "opening-r3.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "5"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "5-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "5-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "5-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "5-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Praise ye the Lord

\score {
  \include "opening-v4.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "6"
  \score {
    \include "opening-v4.ly"
    \midi{ \tempo 4=120 }
  }
}

% The Lord's name be praised

\include "opening-r4.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "7"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "7-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "7-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "7-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "7-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}
