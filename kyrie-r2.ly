global = {
  \key a \dorian
}

soprano = \relative c'' {
  \global
  c2 b4 a b g a a2 \bar ","
  b2 a4 g a b b( c) a2 \bar ","
  c2 b4 a b g e e2 \bar"||"
}

alto = \relative c' {
  \global
  e2 fs4 e e e e e2
  g2 g4 e e g a2 e
  g2 g4 fs fs e c c2
}

tenor = \relative c' {
  \global
  c2 d4 c d d d c2
  d2 c4 b c d d( e) c2
  f2 d4 d d d b a2
}

bass = \relative c' {
  \global
  a2 a4 e g b a a2
  g2 g4 g g g g2 a
  a2 g4 d g b e, e2
}

verse = \lyricmode {
  \all Lord, have mer -- cy up -- on us.
  Christ, have mer -- cy up -- on us.
  Lord, have mer -- cy up -- on us.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
