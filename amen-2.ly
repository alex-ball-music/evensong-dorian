global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'2( c) b1 \bar "||"
}

alto = \relative c' {
  \global
  f1 g
}

tenor = \relative c' {
  \global
  d1 e1
}

bass = \relative c' {
  \global
  a1 a1
}

verse = \lyricmode {
  \all A -- men.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
  >>
>>
