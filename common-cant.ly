\version "2.24.0"

\layout {
  indent = #8
  short-indent = #0
  \context { \Score
    \override RehearsalMark.font-size = #2
  }
  \context { \Voice
    \override DynamicTextSpanner.font-size = #0.5
  }
  \context { \Lyrics
    \override StanzaNumber.font-size = #0.5
    \override LyricText.font-size = #0.5
  }
  \context { \Dynamics
    \override DynamicTextSpanner.font-size = #0.5
  }
}
