<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a4( g a b c d) c( b) a( g) a2 \bar "||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      Praise __ ye the Lord.
    }
  >>
>>
