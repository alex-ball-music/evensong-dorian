global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
   a'4 a g a b c a d( a b) cs2 \bar "||"
}

alto = \relative c' {
  \global
  e4 fs fs g g g g g2( g4) a2
}

tenor = \relative c' {
  \global
  c4 d d e d e d b( d2) e2
}

bass = \relative c' {
  \global
  a4 a a c b a a g( b2) a2
}

verse = \lyricmode {
  \all And make thy cho -- sen peo -- ple joy -- ful.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
