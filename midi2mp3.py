#!/usr/bin/env python3

import os
import sys
import argparse
from tempfile import NamedTemporaryFile
import shutil
import subprocess
from time import strftime
import mido


def concat_midi(args, outfile):
    """Creates the midi outfile by concatenating the list of midi infiles in the
    order specified. Returns true on success and false on failure."""

    # Lilypond uses 384 ticks per beat.
    tpb = 384

    # Set up new midi file
    outmidifile = mido.MidiFile(type=1, ticks_per_beat=tpb)
    metaname = 'Metadata'
    if args.midi:
        metaname = args.midi.replace('.midi', '')
    outmidifile.add_track(metaname)
    end_of_track = 0

    # Process files
    for f, infile in enumerate(args.midifiles):
        inmidifile = mido.MidiFile(infile)
        intracknames = list()
        end_of_last = end_of_track
        this_length = 0
        delta = 0

        # MIDI default tempo is 500000 µs/beat.
        # Code assumes all tempo changes are in the same track.
        tempo = 500000

        # Input MIDI files must be prepared so that Track 0 is metadata
        # (its name is the title of the piece) and all other tracks are
        # consistently named. In Lilypond, this is done by ensuring that each
        # Staff and Lyrics instance is given a label, e.g. `\new Staff = "sa"`.
        # Typical Staff names: sa, tb, soprano, alto, tenor, bass, left, right.
        # Typical Lyrics names: as for Staff, but with `-lyrics` appended.

        # NB. Each msg has time = ticks to wait until reading the next msg.

        # Process each track in this file in turn:
        for i, intrack in enumerate(inmidifile.tracks):
            # First we need to map this track to an output track
            outtrack = None
            intracknames.append(intrack.name)

            if i == 0:
                # Metadata track
                outtrack = outmidifile.tracks[0]
                if len(args.midifiles) == 1:
                    outtrack.name = intrack.name
            else:
                for track in outmidifile.tracks:
                    if track.name == intrack.name:
                        outtrack = track
                        break
                else:
                    outmidifile.add_track(intrack.name)
                    outtrack = outmidifile.tracks[-1]
                    if f:
                        # Not the first input file, so pad start:
                        outtrack.append(mido.MetaMessage(
                            'marker', text='', time=end_of_last))

            msg_time_left = this_length
            eot_found = False

            # Get the messages and add to corresponding track:
            for msg in intrack:
                if msg.type == 'track_name':
                    # No need to have multiple of these:
                    if msg.time:
                        outtrack.append(mido.MetaMessage(
                            'marker', text='replace track name', time=msg.time))
                elif msg.type == 'end_of_track':
                    # Make sure all tracks are set to wait until same end point:
                    eot_found = True
                    if i == 0:
                        # For Lilypond files, msg.time should be track length
                        # and msg_time_left should be 0. For files generated
                        # by this script, msg_time_left should be negative
                        # track length and msg.time should be 0.
                        this_length = msg.time - msg_time_left
                        end_of_track += this_length
                        wait = msg.time
                    else:
                        if msg_time_left < 0:
                            print(f"File {infile}, track {intrack.name}: "
                                  f"expected length {this_length}, but message "
                                  f"waits add up to {0 - msg_time_left}.")
                            sys.exit(1)
                        if msg_time_left != msg.time:
                            print(f"File {infile}, track {intrack.name}: "
                                  f"expected last wait {msg.time}, "
                                  f"actual last wait {msg_time_left}.")
                        wait = msg_time_left
                    if wait:
                        outtrack.append(
                            mido.MetaMessage('marker', text='', time=wait))
                elif msg.type == "set_tempo":
                    tempo = msg.tempo
                    outtrack.append(msg)
                else:
                    outtrack.append(msg)
                msg_time_left -= msg.time

            if not eot_found:
                # Make sure all tracks are set to wait until same end point:
                print(f"File {infile}, track {intrack.name} has no end of "
                      "track message.")
                if msg_time_left > 0:
                    outtrack.append(
                        mido.MetaMessage('marker', text='', time=wait))

        # Pad any unaltered tracks to the right length:
        for i, outtrack in enumerate(outmidifile.tracks):
            if i == 0:
                continue
            if outtrack.name not in intracknames:
                outtrack.append(
                    mido.MetaMessage('marker', text='', time=this_length))

        # After processing all tracks, add inter-file gaps:
        gaptime = args.gap
        if (f + 1) == len(args.midifiles):
            # Last file so use tail time instead
            gaptime = args.tail
        delta = round(mido.second2tick(gaptime, tpb, tempo))
        if delta:
            for outtrack in outmidifile.tracks:
                outtrack.append(mido.MetaMessage('marker', text='', time=delta))
            end_of_track += delta

    # End-of-track markers are added automatically on save.

    # Save new midi file:
    if len(outmidifile.tracks):
        outmidifile.save(outfile)
        return True

    return False


def do_sequential_conversion(args):
    with NamedTemporaryFile(suffix='.midi') as f:
        print("Preparing combined MIDI file...")
        concat_midi(args, f.name)
        if not os.path.isfile(f.name):
            print("Failed to generate intermediate MIDI file.")
            sys.exit(1)

        try:
            if args.midi:
                shutil.copy2(f.name, args.midi)
        except Exception as e:
            print(f"Could not save intermediate MIDI file: {e}")

        if args.out:
            print("Converting MIDI to raw audio...")
            try:
                wav = subprocess.run(
                    f"fluidsynth -l -T raw -F - '{args.soundfont}' '{f.name}'",
                    shell=True, check=True, stdout=subprocess.PIPE)
            except subprocess.CalledProcessError as e:
                print("Failed to convert MIDI to raw audio.")
                print(e)
                sys.exit(1)

            print("Encoding audio as MP3...")
            try:
                subprocess.run(
                    f"lame --preset standard -r - '{args.out}'",
                    shell=True, check=True, input=wav.stdout)
            except subprocess.CalledProcessError as e:
                print("Failed to convert WAV to MP3.")
                print(e)
                sys.exit(1)


def do_streamed_conversion(args):
    with NamedTemporaryFile(suffix='.midi') as f:
        print("Preparing MIDI for conversion...")
        concat_midi(args, f.name)
        if not os.path.isfile(f.name):
            print("Failed to generate intermediate MIDI file.")
            sys.exit(1)

        try:
            if args.midi:
                shutil.copy2(f.name, args.midi)
        except Exception as e:
            print(f"Could not save intermediate MIDI file: {e}")

        if args.out:
            print("Converting MIDI to MP3...")
            wav = subprocess.Popen(
                f"fluidsynth -l -T raw -F - '{args.soundfont}' '{f.name}'",
                shell=True,
                stdout=subprocess.PIPE)

            mp3 = subprocess.Popen(
                f"lame --preset standard -r - '{args.out}'",
                shell=True,
                stdin=wav.stdout)

            wav_rc = wav.wait()
            if wav_rc != 0:
                print("Failed to convert MIDI to WAV"
                      f" (fluidsynth exit status {wav_rc}).")
                sys.exit(1)
            mp3_rc = mp3.wait()
            if mp3_rc != 0:
                print("Failed to convert WAV to MP3"
                      f" (lame exit status {mp3_rc}).")
                sys.exit(1)


def main():
    """Converts MIDI to MP3."""
    parser = argparse.ArgumentParser(
        description="Takes one or more MIDI files and prepares them for "
        "conversion to MP3. According to the options given, the script can "
        "output a single, concatenated and padded MIDI file, and/or an MP3 "
        "generated from this MIDI file.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '-g', '--gap',
        help="seconds to insert between input tracks",
        type=float,
        default=2.0)
    parser.add_argument(
        '-t', '--tail',
        help="seconds of silence to append",
        type=float,
        default=1.0)
    parser.add_argument(
        '-m', '--midi',
        help="path/filename to which to save concatenated MIDI file"
        " (omit to discard this step)",
        type=str)
    parser.add_argument(
        '-o', '--out',
        help="path/filename to which to save MP3 file "
        " (omit to discard this step)",
        type=str)
    parser.add_argument(
        '-p', '--parallel',
        help="generate audio and encode to MP3 in parallel instead of"
             " sequentially",
        action='store_true')
    parser.add_argument(
        '-s', '--soundfont',
        help="path to soundfont to use",
        metavar='SF2',
        type=str,
        default='/usr/share/sounds/sf2/FluidR3_GM.sf2')
    parser.add_argument(
        'midifiles',
        help="MIDI file",
        metavar='MID',
        nargs='+')

    args = parser.parse_args()

    # Check files exist
    has_badfile = False
    for midifile in args.midifiles:
        if not os.path.isfile(midifile):
            print(f"Cannot find file {midifile}.")
            has_badfile = True

    if not os.path.isfile(args.soundfont):
        print(f"Cannot find soundfont {args.soundfont}.")
        has_badfile = True

    if has_badfile:
        print("Please check before continuing.")
        sys.exit(1)

    # Run operations
    if args.parallel:
        do_streamed_conversion(args)
    else:
        do_sequential_conversion(args)

    # Finish
    if args.out:
        print(f"MP3 file {args.out} created successfully.")


if __name__ == "__main__":
    main()
