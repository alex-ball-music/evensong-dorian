global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 a g a b c b c d c c b( g) a2 \bar "||"
}

alto = \relative c' {
  \global
  e4 e e e e g g fs fs g g e2 e2
}

tenor = \relative c' {
  \global
  c4 b b a a g g a b b a g( b) c2
}

bass = \relative c' {
  \global
  a4 g g fs fs e e d d e e e2 a2
}

verse = \lyricmode {
  \all And mer -- ci -- ful -- ly hear us when we call up -- on thee.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
