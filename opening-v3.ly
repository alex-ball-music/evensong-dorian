<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a1 g4 a \bar"'" b c b a2 \bar"'" a4 a a b( a) g a2 \bar "||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      "Glory be to the" Fa -- ther, and to the Son, and to the Ho -- ly Ghost.
    }
  >>
>>
