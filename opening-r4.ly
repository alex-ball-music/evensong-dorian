global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 d2 a4 b cs2 \bar "||"
}

alto = \relative c' {
  \global
  e4 g2 g4 g a2
}

tenor = \relative c' {
  \global
  c4 b2 d4 d e2
}

bass = \relative c' {
  \global
  a4 g2 b4 b a2
}

verse = \lyricmode {
  \all The Lord’s name be praised
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
