global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 c2 b4( c) b( a) g a2 a \bar "||"
}

alto = \relative c' {
  \global
  e4 fs2 g2 g4( e) e e2 e
}

tenor = \relative c' {
  \global
  c4 d2 e2 d b4 d2 c
}

bass = \relative c' {
  \global
  a4 a2 a2 g2 e4 a2 a
}

verse = \lyricmode {
  \all O Lord, make haste to help us.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
