global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 a b2 c4 d c( b) a( g) a2 \bar "||"
}

alto = \relative c' {
  \global
  e4 e e( fs) g4 g g2 e e
}

tenor = \relative c' {
  \global
  c4 d e2 e4 b c( d) c4( b) d2
}

bass = \relative c' {
  \global
  a4 fs g( fs) e4 e g2 a4( e) a2
}

verse = \lyricmode {
  \all And our \melis mouth shall shew forth thy praise.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
