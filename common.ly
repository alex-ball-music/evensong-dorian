\version "2.24.0"
\language "english"

%%% Useful tests for footers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define (no-tagline layout props arg)
  (if (not (chain-assoc-get 'header:tagline props #f))
      (interpret-markup layout props arg)
      empty-stencil))
#(define (book-last-page? layout props)
   "Return #t iff the current page number, got from @code{props}, is the
book last one."
   (and (chain-assoc-get 'page:is-bookpart-last-page props #f)
        (chain-assoc-get 'page:is-last-bookpart props #f)))
#(define (not-last-page layout props arg)
  (if (not (book-last-page? layout props))
      (interpret-markup layout props arg)
      empty-stencil))

%%% Extra barline functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Stock functions from bar-line.scm
#(define (calc-blot thickness extent grob)
  "Calculate the blot diameter by taking @code{'rounded}
and the dimensions of the extent into account."
  (let* ((rounded (ly:grob-property grob 'rounded #f))
         (blot (if rounded
                   (let ((blot-diameter (layout-blot-diameter grob))
                         (height (interval-length extent)))

                     (cond ((< thickness blot-diameter) thickness)
                           ((< height blot-diameter) height)
                           (else blot-diameter)))
                   0)))

    blot))
#(define (staff-symbol-line-count staff)
  "Get or compute the number of lines of staff @var{staff}."
  (let ((line-count 0))

    (if (ly:grob? staff)
        (let ((line-pos (ly:grob-property staff 'line-positions '())))

          (set! line-count (if (pair? line-pos)
                               (length line-pos)
                               (ly:grob-property staff 'line-count 0)))))

    line-count))
#(define (get-staff-symbol grob)
  "Return the staff symbol corresponding to Grob @var{grob}."
  (if (grob::has-interface grob 'staff-symbol-interface)
      grob
      (ly:grob-object grob 'staff-symbol)))

% Replacement for 'tick' bar line, slightly thicker
#(define (make-tick-bar-line grob extent)
  "Draw a tick bar line."
  (let* ((half-tick (* 2/3 (ly:staff-symbol-staff-space grob)))
         (line-thickness (layout-line-thickness grob))
         (thickness (* (ly:grob-property grob 'hair-thickness 1)
                       line-thickness))
         (height (interval-end extent))
         (blot (calc-blot thickness extent grob)))

    (ly:round-filled-box (cons 0 thickness)
                         (cons (- height half-tick) (+ height half-tick))
                         blot)))
#(add-bar-glyph-print-procedure "'" make-tick-bar-line)
#(define-bar-line "'" "'" #f #f)

% New function for 'middle' bar line, with gaps top and bottom
#(define (make-middle-bar-line grob extent)
  "Draw a two-thirds bar line."
  (let* ((line-thickness (layout-line-thickness grob))
         (thickness (* (ly:grob-property grob 'hair-thickness 1)
                       line-thickness))
         (half-height (* 1/2 (+ (interval-start extent) (interval-end extent))))
         (staff-space (ly:staff-symbol-staff-space grob))
         (staff-symbol (get-staff-symbol grob))
         (gap-count (- (staff-symbol-line-count staff-symbol) 1))
         (third-height (* 1/3 (* staff-space gap-count)))
         (blot (calc-blot thickness extent grob)))

    (ly:round-filled-box (cons 0 thickness)
                         (cons (- half-height third-height) (+ half-height third-height))
                         blot)))
#(add-bar-glyph-print-procedure "," make-middle-bar-line)
#(define-bar-line "," "," #f #f)

%%% MIDI equalizer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define choral-instrument-equalizer-alist '())

#(set! choral-instrument-equalizer-alist
  (append
    '(
      ("choir aahs" . (0.3 . 1.0))
      ("church organ" . (0.1 . 0.8)))
    choral-instrument-equalizer-alist))

#(define (choral-instrument-equalizer s)
  (let ((entry (assoc s choral-instrument-equalizer-alist)))
    (if entry
      (cdr entry))))

%%% Shorthands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sh = {
  \skip4
}

all = \override Lyrics.LyricText.font-series = #'bold

cantor = \override Lyrics.LyricText.font-series = #'medium

melis = \once \override LyricText.self-alignment-X = #LEFT

#(define-markup-command (smcp layout props text) (markup?)
  "Uses true small caps for text."
  (interpret-markup layout props
    (markup #:override '(font-features . ("smcp")) text)))

%%% Common layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\layout {
  \context { \Score
    \override BarNumber.font-shape = #'italic
    \override BarNumber.padding = #2.0
    \override RehearsalMark.stencil
      = #(make-stencil-boxer 0.1 0.25 ly:text-interface::print)
  }
  \context { \Staff
    \consists "Merge_rests_engraver"
    \RemoveAllEmptyStaves
    \numericTimeSignature
    \accidentalStyle neo-modern-voice
    \override VerticalAxisGroup.minimum-Y-extent = #'(-3 . 4)
    \override Parentheses.font-size = #2
  }
  \context { \Voice
    \consists "Melody_engraver"
    \override Stem.neutral-direction = #'()
    \override Flag.stencil = #modern-straight-flag
  }
  \context {
    \Lyrics
    % Fonts are sized to give 11pt text at staffsize 19
    \override VerticalAxisGroup.minimum-Y-extent = #'(-0 . 0)
    \override VerticalAxisGroup.nonstaff-relatedstaff-spacing = #'((padding . 0.5))
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing = #'((padding . 0.5))
    \override LyricSpace.minimum-distance = #1.0
  }
}

%%% Rehearsal MIDI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rehearsalMidi = #
(define-music-function
 (parser location name midiInstrument lyrics) (string? string? ly:music?)
 #{
   \unfoldRepeats <<
     \new Staff = "soprano" \new Voice = "soprano" { \sopranoFull }
     \new Staff = "alto" \new Voice = "alto" { \altoFull }
     \new Staff = "tenor" \new Voice = "tenor" { \tenorFull }
     \new Staff = "bass" \new Voice = "bass" { \bassFull }
     \context Staff = $name {
       \set Score.midiMinimumVolume = #0.5
       \set Score.midiMaximumVolume = #0.5
       \set Score.midiInstrument = "clav"
       \set Staff.midiMinimumVolume = #0.8
       \set Staff.midiMaximumVolume = #1.0
       \set Staff.midiInstrument = $midiInstrument
     }
     \new Lyrics = $(string-append name "-lyrics") \with {
       alignBelowContext = $name
     } \lyricsto $name $lyrics
     \pianoPart
     \context Staff = "right" {
       \set Staff.midiMinimumVolume = #0.3
       \set Staff.midiMaximumVolume = #0.3
       \set Staff.midiInstrument = "synthbrass 1"
     }
     \context Staff = "left" {
       \set Staff.midiMinimumVolume = #0.3
       \set Staff.midiMaximumVolume = #0.3
       \set Staff.midiInstrument = "synthbrass 1"
     }
   >>
 #})

rehearsalMidiChoir = #
(define-music-function
 (parser location name midiInstrument lyrics) (string? string? ly:music?)
 #{
   \unfoldRepeats <<
     \new Staff = "soprano" \new Voice = "soprano" { \soprano }
     \new Staff = "alto" \new Voice = "alto" { \alto }
     \new Staff = "tenor" \new Voice = "tenor" { \tenor }
     \new Staff = "bass" \new Voice = "bass" { \bass }
     \context Staff = $name {
       \set Score.midiMinimumVolume = #0.5
       \set Score.midiMaximumVolume = #0.5
       \set Score.midiInstrument = "clav"
       \set Staff.midiMinimumVolume = #0.8
       \set Staff.midiMaximumVolume = #1.0
       \set Staff.midiInstrument = $midiInstrument
     }
     \new Lyrics = $(string-append name "-lyrics") \with {
       alignBelowContext = $name
     } \lyricsto $name $lyrics
   >>
 #})
