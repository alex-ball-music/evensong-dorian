global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  e4 e g \bar"'" a b a c2 \bar"," b4 a a g a2 \bar","
  a4 c( b) a b2 \bar"," b4 b c a \bar"'" a e g a b a c2 \bar","
  a4 b a c b a g a2 \bar"," b4 g a b a g fs e \bar"'"
  e4 a g a b a g fs e a b2 \bar","
  e,4 g a c b a g a a2 \bar"," b4 a d a a g e e2 \bar","
  \lpEndS}

alto = \relative c' {
  \global
  e4 e g a g fs g2 g4 e e e e2
  e4 a( g) fs fs2 fs4 g g fs fs e e e g fs g2
  fs4 g f g g e e e2 g4 e f \accidentalStyle modern-voice f f e e c \accidentalStyle neo-modern-voice
  c4 d e f \accidentalStyle modern-voice f f e e c \accidentalStyle neo-modern-voice d d2
  e4 e fs g g e e e e2 e4 e fs e d e d c2
  \lpEndA
}

tenor = \relative c {
  \global
  e4 e g a b d e2 d4 c b b d( cs)
  c4 f( d) d d2 d4 d e d d b b c d d e2
  d4 d c e d b b cs2 d4 b c c a b b b
  a4 a b c c a b b a a b2
  b4 b d e d b b d c2 b4 d a c b b g g2
  \lpEndT
}

bass = \relative c {
  \global
  e4 e g a g a c2 g4 a e e a2
  a4 f( g) a b2 a4 g c, d d e e e g a c2
  d4 g, a c g e e a2 g4 g f \accidentalStyle modern-voice f f e e e \accidentalStyle neo-modern-voice
  e4 fs g f \accidentalStyle modern-voice f f e e e \accidentalStyle neo-modern-voice d g2
  e4 e d c g' e e a a2 g4 fs? d e e e c c2
  \lpEndB
}

verse = \lyricmode { \all
  Our Fa -- ther, which art in heav’n,
  hal -- lowed be thy name;
  thy king -- dom come;
  thy will be done,
  in earth as it is in heav’n.
  Give us this day our dai -- ly bread.
  And for -- give us our tres -- pass -- es,
  as we for -- give them that tres -- pass a -- gainst us.
  And lead us not in -- to temp -- ta -- tion;
  but de -- li -- ver us from e -- vil.
  \lpEndL
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
