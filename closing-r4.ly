global = {
  \key a \dorian
}

soprano = \relative c' {
  \global
  a'4 c2 b4 c b g a2 \bar "||"
}

alto = \relative c' {
  \global
  e4 fs2 g4 g g e e2
}

tenor = \relative c' {
  \global
  c4 d2 e4 e d b d2
}

bass = \relative c' {
  \global
  a4 a2 a4 a g g a2
}

verse = \lyricmode {
  \all And bless thine in -- her -- it -- ance.
}

choirPart = <<
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef treble
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \override NoteHead.style = #'diamond \alto }
  >>
  \new Lyrics = "lyrics" \lyricsto "soprano" \verse
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \override NoteHead.style = #'diamond \bass }
  >>
>>
