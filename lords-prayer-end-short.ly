lpEndS = \relative c'' {
  c4( b) a( g a2) \bar"||"
}

lpEndA = \relative c'' {
  g2 fs4( e e2)
}

lpEndT = \relative c' {
  e2 b2( cs2)
}

lpEndB = \relative c {
  e4( g) e2( a2)
}

lpEndL = \lyricmode {
  A -- men.
}