\version "2.20.0"
\include "common.ly"
\include "common-test.ly"

\header {
  title = "Magnificat"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\include "gloria.ly"
\include "mag.ly"

\score {
  <<
    \choirPart
    \pianoPart
    \context Staff {
      \set Score.instrumentEqualizer = #choral-instrument-equalizer
    }
  >>
  \layout { }
  \midi { }
}

% Rehearsal MIDI files:
sopranoFull = { \soprano \sgloria }
altoFull = { \alto \agloria }
tenorFull = { \tenor \tgloria }
bassFull = { \bass \bgloria }

saLibretto = { \saVerse \sagloriaVerse }

tbfullVerse = \lyricmode {
  % Lyrics follow here.
  My soul doth mag -- ni -- fy the Lord,
  and my spir -- it hath re -- joiced in God my Sa -- viour.

  For be -- hold, from hence -- forth
  all gen -- er -- a -- tions shall call me blest.
  For he that is migh -- ty hath mag -- ni -- fied me
  and ho -- ly is his Name.

  He hath shewed strength with his arm;
  he hath scat -- tered the proud in the i -- ma -- gin -- a -- tion of their hearts.

  He hath put down the migh -- ty from their seat,
  and hath ex -- al -- ted the hum -- ble and meek.
  He hath filled the hun -- gry with good things,
  and the rich he hath sent emp -- ty a -- way.

  He re -- mem -- ber -- ing his mer -- cy hath holp -- en his ser -- vant Is -- ra -- el,
  as he prom -- ised to our fore -- fath -- ers, Ab -- ra -- ham and his seed for
  e -- ver, e -- ver, e -- ver -- more.
}

tbLibretto = { \tbfullVerse \tbgloriaVerse }

\book {
  \bookOutputSuffix "soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \saLibretto
    \midi { }
  }
}

\book {
  \bookOutputSuffix "alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \saLibretto
    \midi { }
  }
}

\book {
  \bookOutputSuffix "tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tbLibretto
    \midi { }
  }
}

\book {
  \bookOutputSuffix "bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \tbLibretto
    \midi { }
  }
}
