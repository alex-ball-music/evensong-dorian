\version "2.20.0"
\include "common.ly"
\include "common-resp.ly"

\header {
  title = "Closing Prayers and Preces"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\markup { \bold {Kyrie and Paternoster} }

% The Lord be with you

\score {
  \include "kyrie-v1.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "00"
  \score {
    \include "kyrie-v1.ly"
    \midi{ \tempo 4=120 }
  }
}

% And with thy spirit

\include "kyrie-r1.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "01"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "01-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "01-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "01-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "01-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Let us pray

\score {
  \include "kyrie-v2.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "02"
  \score {
    \include "kyrie-v2.ly"
    \midi{ \tempo 4=120 }
  }
}

% Kyrie Eleison

\include "kyrie-r2.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "03"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "03-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "03-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "03-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "03-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Lord's Prayer (short)

\include "lords-prayer-end-short.ly"
\include "lords-prayer.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "04"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "04-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "04-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "04-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "04-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\markup { \bold {Closing Preces} }

% O Lord, shew thy mercy...

\score {
  \include "closing-v1.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "05"
  \score {
    \include "closing-v1.ly"
    \midi{ \tempo 4=120 }
  }
}

% And grant us...

\include "closing-r1.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "06"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "06-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "06-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "06-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "06-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% O Lord, save the Queen

\score {
  \include "closing-v2.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "07"
  \score {
    \include "closing-v2.ly"
    \midi{ \tempo 4=120 }
  }
}

% And mercifully hear us...

\include "closing-r2.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "08"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "08-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "08-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "08-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "08-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Endue thy ministers...

\score {
  \include "closing-v3.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "09"
  \score {
    \include "closing-v3.ly"
    \midi{ \tempo 4=120 }
  }
}

% And make thy chosen...

\include "closing-r3.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "10"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "10-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "10-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "10-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "10-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% O Lord, save thy people

\score {
  \include "closing-v4.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "11"
  \score {
    \include "closing-v4.ly"
    \midi{ \tempo 4=120 }
  }
}

% And bless thine inheritance

\include "closing-r4.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "12"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "12-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "12-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "12-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "12-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Give peace...

\score {
  \include "closing-v5.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "13"
  \score {
    \include "closing-v5.ly"
    \midi{ \tempo 4=120 }
  }
}

% Because there is none other...

\include "closing-r5.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "14"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "14-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "14-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "14-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "14-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% O God, make clean...

\score {
  \include "closing-v6.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "15"
  \score {
    \include "closing-v6.ly"
    \midi{ \tempo 4=120 }
  }
}

% And take not...

\include "closing-r6.ly"

\score {
  <<
    \choirPart
  >>
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "16"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "16-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "16-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "16-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "16-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\markup { \bold {Collects} }

% Collect 1

collect = <<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a1 g4 a2 \bar "||"
      }
    }
    \new Lyrics = "soprano-lyrics" \lyricsto "soprano" {
      ". . . for" e -- ver.
    }
  >>
>>

\score {
  \collect
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "17"
  \score {
    \collect
    \midi{ \tempo 4=120 }
  }
}

\include "amen-1.ly"

\score {
  <<
    \choirPart
  >>
}

\book {
  \bookOutputSuffix "18"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "18-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "18-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "18-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "18-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Collect 2

collect = <<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a1 g4 a2 \bar "||"
      }
    }
    \new Lyrics = "soprano-lyrics" \lyricsto "soprano" {
      ". . . our" Sav -- iour.
    }
  >>
>>

\score {
  \collect
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "19"
  \score {
    \collect
    \midi{ \tempo 4=120 }
  }
}

\include "amen-2.ly"

\score {
  <<
    \choirPart
  >>
}

\book {
  \bookOutputSuffix "20"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "20-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "20-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "20-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "20-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

% Collect 3

collect = <<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c'' {
        a1 g4 a2 \bar "||"
      }
    }
    \new Lyrics = "soprano-lyrics" \lyricsto "soprano" {
      ". . . Je" -- sus Christ.
    }
  >>
>>

\score {
  \collect
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "21"
  \score {
    \collect
    \midi{ \tempo 4=120 }
  }
}

\include "amen-3.ly"

\score {
  <<
    \choirPart
  >>
}

\book {
  \bookOutputSuffix "22"
  \score {
    <<
      \choirPart
    >>
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "22-soprano"
  \score {
    \rehearsalMidiChoir "soprano" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "22-alto"
  \score {
    \rehearsalMidiChoir "alto" "soprano sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "22-tenor"
  \score {
    \rehearsalMidiChoir "tenor" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}

\book {
  \bookOutputSuffix "22-bass"
  \score {
    \rehearsalMidiChoir "bass" "tenor sax" \verse
    \midi { \tempo 4=120 }
  }
}
