\version "2.20.0"
\include "common.ly"
\include "common-cant.ly"

\include "gloria.ly"
\include "mag.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
