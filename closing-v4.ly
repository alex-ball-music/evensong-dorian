<<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c' {
        e4 g( a) \bar"'" a( g) a b( c) a2 \bar "||"
      }
    }
    \new Lyrics = "lyrics" \lyricsto "soprano" {
      O Lord, save thy peo -- ple.
    }
  >>
>>
